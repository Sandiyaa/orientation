**DOCKER**

* Docker is an open platform for developing, shipping, and running applications.
* Docker enables you to separate your applications from your infrastructure so you can deliver software quickly.
* With Docker, you can manage your infrastructure in the same ways you manage your applications.
* By taking advantage of Docker’s methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.

![](https://www.docker.com/sites/default/files/social/docker_facebook_share.png)

**DOCKER ENGINE**
- Docker Engine is an open source containerization technology for building and containerizing your applications. 
- Docker Engine acts as a client-server application with: A server with a long-running daemon process dockerd . 
- APIs which specify interfaces that programs can use to talk to and instruct the Docker daemon.

![](https://zdnet4.cbsistatic.com/hub/i/r/2016/12/01/2c7497f8-81ec-4e18-b388-6328156f5b40/resize/770xauto/d21f00c6f7d666dc052c4b91a6d5e0ae/docker-engine.jpg)

**DOCKER IMAGE AND CONTAINERS**
- A Docker image is a file, comprised of multiple layers, that is used to execute code in a Docker container. 
- An image is essentially built from the instructions for a complete and executable version of an application, which relies on the host OS kernel.
- A Docker container is nothing but a running process, with some added encapsulation features applied to it in order to keep it isolated from the host and from other containers.

![docker img](https://geekflare.com/wp-content/uploads/2019/07/dockerfile-697x270.png)


**CONTAINER Vs VM**
- Docker is container based technology and containers are just user space of the operating system. 
- At the low level, a container is just a set of processes that are isolated from the rest of the system, running from a distinct image that provides all files necessary to support the processes.
- It is built for running applications.
- In Docker, the containers running share the host OS kernel.
- A Virtual Machine, on the other hand, is not based on container technology.
- They are made up of user space plus kernel space of an operating system.
- Under VMs, server hardware is virtualized. Each VM has Operating system (OS) & apps. It shares hardware resource from the host.

![](https://www.backblaze.com/blog/wp-content/uploads/2018/06/whats-the-diff-container-vs-vm.jpg)

- VMs & Docker – each comes with benefits and demerits. Under a VM environment, each workload needs a complete OS. But with a container environment, multiple workloads can run with 1 OS. - The bigger the OS footprint, the more environment benefits from containers. With this, it brings further benefits like Reduced IT management resources, reduced size of snapshots, quicker spinning up apps, reduced & simplified security updates, less code to transfer, migrate and upload workloads.
 
- A Docker image includes everything needed to run an application - the code or binary, runtimes, dependencies, and any other filesystem objects required.

**DOCKER PLATFORM**

* Docker provides the ability to package and run an application in a loosely isolated environment called a container.
* The isolation and security allow you to run many containers simultaneously on a given host.
* Containers are lightweight because they don’t need the extra load of a hypervisor, but run directly within the host machine’s kernel.
* This means you can run more containers on a given hardware combination than if you were using virtual machines. 
* You can even run Docker containers within host machines that are actually virtual machines!
Docker provides tooling and a platform to manage the lifecycle of your containers:

![](https://i2.wp.com/blog.docker.com/wp-content/uploads/2018/10/56b18e16-a4c3-42f4-acd1-b962aed0ce17-1.jpg?w=1140&ssl=1)

- Develop your application and its supporting components using containers.
- The container becomes the unit for distributing and testing your application.
- When you’re ready, deploy your application into your production environment, as a container or an orchestrated service. This works the same whether your production environment is a local data center, a cloud provider, or a hybrid of the two.

**INSTALLATION OF DOCKER**

- Uninstall the older version of docker if is already installed.
```sh
$ sudo apt-get remove docker docker-engine docker.io containerd runc
```
- Installing the new version of docker is preferrable.

```sh
     $ sudo apt-get update
     $ sudo apt-get install \
         apt-transport-https \
         ca-certificates \
         curl \
         gnupg-agent \
         software-properties-common
     $ curl -fsSL https://download.docker.com/linux   /ubuntu/gpg | sudo apt-key add -
     $ sudo apt-key fingerprint 0EBFCD88
     $ sudo add-apt-repository \
        "deb [arch=amd64] https://download.docker.com  /linux/ubuntu \
        $(lsb_release -cs) \
        stable nightly test"
     $ sudo apt-get update
     $ sudo apt-get install docker-ce docker-ce-cli  containerd.io

     // Check if docker is successfully installed  in your system
     $ sudo docker run hello-world
```

**DOCKER PHRASEOLOGIES**

- Docker
- Docker image
- Container
- Docker Hub
![](https://www.sdxcentral.com/wp-content/uploads/2019/05/ContainersvsVMs_Image-1.jpg)

**BASIC COMMANDS IN DOCKER**

- docker ps
- docker start
- docker stop
- docker run
- docker rm

![](https://wiki.aquasec.com/download/attachments/2854889/Docker_Architecture.png?version=1&modificationDate=1520172700553&api=v2)

**COMMON OPERATIONS ON DOCKER**

![](https://miro.medium.com/max/4980/1*5ODaw4rEGUjFGgMMH93yVw.png)

Typically, you will be using docker in the given flow,

- Download/pull the docker images that you want to work with.
- Copy your code inside the docker
- Access docker terminal
- Install and additional required dependencies
- Compile and Run the Code inside docker
- Document steps to run your program in README.md file
- Commit the changes done to the docker.
- Push docker image to the docker-hub and share repository with people who want to try your code.


